﻿using System;
using System.Diagnostics;

namespace UltimateSandwich
{
    public sealed class Dice
    {
        private static Random rd;
        
        private int n;
        private int d;
        private int m;

        static Dice()
        {
            rd = new Random();
        }

        /// <summary>
        /// nDd + m 다이스를 굴린다
        /// </summary>
        public Dice(int n, int d, int m)
        {
            this.n = n;
            this.d = d;
            this.m = m;

            Debug.Assert(n == 0 || d > 1);
        }

        public int Roll()
        {
            int sum = 0;
            for (int diceCount = 0; diceCount < n; diceCount++)
            {
                int rolled = rd.Next(0, d) + 1;
                if (rolled == d)
                    diceCount--;

                sum += rolled;
            }

            return Math.Max(0, sum + m);
        }
    }
}
