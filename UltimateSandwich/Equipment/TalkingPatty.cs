﻿namespace UltimateSandwich.Equipment
{
    public class TalkingPatty : EquipmentBase
    {
        public TalkingPatty()
            : base(EquipmentSlot.Meat, "파인애플에 관해 의견이 있는 패티", 0, 0, 0)
        {
            //0, 0, 0을 다른 숫자로 바꿔야
        }
    }
}
