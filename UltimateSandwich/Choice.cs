﻿using System;

namespace UltimateSandwich
{
    public struct Choice
    {
        public String Description;
        public Action Result;

        public Choice (string description, Action result)
        {
            Description = description;
            Result = result;
        }
    }
}
