﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using UltimateSandwich.Unit;

namespace UltimateSandwich
{
    public sealed class Game
    {
        public const int MapWidth = 40;
        public const int MapHeight = 20;

        private const int MaxMessageLine = 30;

        private static Game instance;
        private static Random rd;

        private Map currentMap;

        public Player Player;
        private List<String> messages;

        private int Turn;

        static Game()
        {
            rd = new Random();
        }
        public Game()
        {
            try
            {
                Console.SetWindowSize(120, 60);
            }
            catch { }

            Debug.Assert(Game.instance == null);
            Game.instance = this;

            messages = new List<String>();
            Player = new Player();

            MoveToMap(Map.CreateNormalMap());

            Console.CursorVisible = false;
        }

        private void DisplayHUD()
        {
            int maxWidth = 20;
            int width;

            int startLeft = 82;

            Console.SetCursorPosition(startLeft, 0);
            Console.Write("".PadRight(Console.WindowWidth - startLeft));

            Console.SetCursorPosition(startLeft, 0);
            Console.ForegroundColor = ConsoleColor.White;
            width = maxWidth * Player.HP / Player.MaxHP;

            if (width < 0)
                width = 0;
            if (width == 0 && Player.HP > 0)
                width = 1;

            Console.Write("체력 [");

            if (width < maxWidth / 5)
                Console.BackgroundColor = ConsoleColor.Red;
            else if (width < maxWidth / 2)
                Console.BackgroundColor = ConsoleColor.Yellow;
            else
                Console.BackgroundColor = ConsoleColor.Green;

            Console.Write("{0}", "".PadRight(width, '#'));

            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;

            Console.Write("{0}] ", "".PadRight(maxWidth - width, ' '));
            Console.Write("{0}/{1}", Player.HP, Player.MaxHP);

            Console.SetCursorPosition(startLeft, 10);
            Console.Write("현재 층의 유닛들");

            List<GameObject> distinctList = new List<GameObject>();
            foreach (GameObject obj in currentMap.ObjectList)
                if (distinctList.Exists(_ => _.GetType() == obj.GetType()) == false)
                    distinctList.Add(obj);

            for (int i = 0; i < 9; i++)
            {
                if (distinctList.Count > i)
                {
                    GameObject obj = distinctList[i];
                    if (obj == Player)
                        continue;
                    if (obj is Wall)
                        continue;
                    if (obj is Stair)
                        continue;

                    Console.SetCursorPosition(startLeft, Console.CursorTop + 1);
                    obj.Display();

                    Console.ResetColor();
                    Console.Write(" ");
                    Console.Write(obj.RealName);
                }
                else
                {
                    Console.SetCursorPosition(startLeft, Console.CursorTop + 1);
                    Console.Write(new String(' ', Console.WindowWidth - startLeft));
                }
            }
        }

        private void Display()
        {
            Console.ResetColor();
            Console.SetCursorPosition(0, 0);

            for (int y = Game.MapHeight - 1; y >= 0; y--)
            {
                for (int x = 0; x < Game.MapWidth; x++)
                {
                    if (Game.GetByPosition(x, y) != null)
                        Game.GetByPosition(x, y).Display();
                    else
                    {
                        Console.ResetColor();
                        Console.Write("　");
                    }
                }

                Console.WriteLine();
            }

            Console.ResetColor();
            Console.WriteLine();
            int messageCount = Math.Min(messages.Count, Game.MaxMessageLine);
            for (int i = 0; i < messageCount; i++)
            {
                Console.Write(messages[messages.Count - messageCount + i]);
                Console.Write(new String(' ', Console.WindowWidth - Console.CursorLeft));
            }
        }
        private void DisplayTitle()
        {
            Console.WriteLine(@"     
    _______  __   __  _______                                                                                       
   |       ||  | |  ||       |                                                                                      
   |_     _||  |_|  ||    ___|                                                                                      
     |   |  |       || 궁|___                                                                                       
     |   |  |   _   ||    ___|                                                                                      
     |   |  |  | |  ||   |___                                                                                       
     |___|  |__| |__||_______|                                                                                      
                         __   __  ___      _______  ___   __   __  _______  _______  _______                         
                        |  | |  ||   |    |       ||   | |  | |  ||   _   ||       ||       |                        
                        |  | |  ||   |    |_     _||   | |  |_|  ||  | |  ||_     _||    ___|                        
                        |  | |  ||   |      |   |  |   | |  의   ||  | |  |  |   |  |   |___                         
                        |  |_|  ||   |___   |   |  |   | |       ||  |_|  |  |   |  |    ___|                        
                        |  극   ||       |  |   |  |   | | ||_|| ||   _   |  |   |  |   |___                         
                        |_______||_______|  |___|  |___| |_|   |_||__| |__|  |___|  |_______|                        
                                                 _______  _______  __    _  ______   _     _  ___   _______  __   __ 
                                                |   샌  ||   _   ||  |  | ||  드  | | | _ | ||   | |   ____||  | |  |
                                                |  _____||  | |  ||   |_| ||  _    || || || ||   | |  |     |  |_|  |
                                                | |_____ |  | |  ||       || | |   || ||  | ||   | |  |     |   치  |
                                                |_____  ||  |_|  ||  _    || |_|   ||   위  ||   | |  |     |   _   |
                                                 _____| ||   _   || | |   ||       ||   _   ||   | |  |____ |  | |  |
                                                |_______||__| |__||_|  |__||______| |__| |__||___| |_______||__| |__|");

            Console.ReadKey(true);
        }
        private void DisplayIntro()
        {
            Console.Clear();

            Console.WriteLine("생태계에서 가장 중요한 것은 언제나 균형이다.");
            Console.WriteLine("패스트푸드 생태계도 마찬가지다. 버거킹, 맥도날드, 맘스터치, 서브웨이, 파파이스, ... 이들 패스트푸드는 모두 서로와 때로는 어울리며, 때로는 겨루며 사람들의 혀를 즐겁게 하고 있었다.");
            Console.WriteLine();

            Console.WriteLine("어느 날 나타난 패스트푸드 세상에 나타난 롯데리아는 이 균형을 무시하고, 비웃었고, 박살내며 모두를 가차없이 공격했다. 기존 패스트푸드들은 격노해 맞서 싸웠으나, 압도적인 점포 수를 바탕으로 끝없이 몰려온 롯데리아 메뉴의 맛없음에 이내 굴복하고 말았다.");
            Console.WriteLine();

            Console.WriteLine("그러나 롯데리아의 폭거에 맞서다 숨은 이들 가운데 아직 희망이 남아 있었으니, 그 이름은 전설의 고기, 전설의 빵, 전설의 양상추, 그리고 전설의 소스.");
            Console.Write("서브웨이는 마지막 힘을 짜내 기본 샌드위치를 하나를 만들어, 전설로 남은 이들이 숨은 곳을 찾아가 함께 힘을 합쳐 롯데리아의 수장 ");
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.Write("AZ버거");
            Console.ResetColor();
            Console.WriteLine("를 무찌르라는 사명을 남기고 목숨을 다했다.");
            Console.WriteLine();

            Console.WriteLine("이제 이 마지막 샌드위치의 모험이 시작된다...");

            Console.SetCursorPosition(0, 28);
            Console.WriteLine("아무 키를 눌러서 진행");

            Console.ReadKey(true);
        }

        public void Run()
        {
            DisplayTitle();
            DisplayIntro();
            Console.Clear();

            while (true)
            {
                Display();
                DisplayHUD();

                Player.Update();
                foreach (GameObject obj in currentMap.ObjectList)
                    if (obj != Player && obj.HP > 0)
                        obj.Update();

                currentMap.ObjectList.RemoveAll(_ => _.HP <= 0);
                Turn++;
            }
        }

        private void WriteMessage(String s)
        {
            String[] splitted = s.Replace("\r", "").Split('\n');

            messages.Add(String.Format("턴 {0}: {1}", Turn.ToString().PadLeft(4, ' '), splitted[0]));
            for (int i = 1; i < splitted.Length; i++)
                if (String.IsNullOrWhiteSpace(splitted[i]) == false)
                    messages.Add(String.Format("    {0} {1}", "".PadLeft(4, ' '), splitted[i]));
        }

        public static void WriteMessage(String format, params Object[] args)
        {
            Game.instance.WriteMessage(String.Format(format, args));
            Game.instance.Display();
        }
        public static GameObject GetByPosition(int x, int y)
        {
            return Game.instance.currentMap.GetByPosition(x, y);
        }

        public static Player GetPlayer()
        {
            return Game.instance.Player;
        }
        public static void MoveToMap(Map m)
        {
            Game.instance.currentMap = m;
            Game.instance.currentMap.ObjectList.Add(Game.GetPlayer());
            Game.GetPlayer().GotoRandomPosition();
        }

        public static Map GetMap()
        {
            return Game.instance.currentMap;
        }
        public static void AddUnit(GameObject obj)
        {
            Game.instance.currentMap.ObjectList.Add(obj);
        }
    }
}
