﻿using System;
using System.Collections.Generic;
using UltimateSandwich.Unit;

namespace UltimateSandwich
{
    public sealed class Player : GameObject
    {
        public List<Item> Inventory = new List<Item>();

        public EncounterBase CurrentEncounter;
        public Dictionary<EquipmentSlot, EquipmentBase> EquippedItems
            = new Dictionary<EquipmentSlot, EquipmentBase>()
            {
                { EquipmentSlot.Bread, null },
                { EquipmentSlot.Cabbage, null },
                { EquipmentSlot.Meat, null },
                { EquipmentSlot.Sauce, null },
            };

        public Player()
        {
            Foreground = ConsoleColor.Black;
            Background = ConsoleColor.White;
            DisplayName = "나";
            RealName = "당신";

            HP = 400;
            MaxHP = 400;

            ToHitDice = new Dice(2, 6, 3);
            DamageDice = new Dice(2, 6, 3);
        }

        public override void Chat()
        {
            Game.WriteMessage("당신은 자기 자신에게 말을 걸었다.");
        }
        public override void Update()
        {
            ConsoleKeyInfo key;
            // 자세한 스텟이나 상태 보기 파트

            // 인카운터 시 선택지 파트
            if (CurrentEncounter != null)
            {
                CurrentEncounter.BeginEncounter();
                CurrentEncounter.DisplayChoices();
                key = Console.ReadKey(true);
                bool valid = CurrentEncounter.GiveChoice(key.Key);
                if (valid == true)
                    CurrentEncounter = null;
                return;
            }

            key = Console.ReadKey(true);
            // 행동 파트
            switch (key.Key)
            {
                case ConsoleKey.NumPad1:
                    PlayerMove(-1, -1);
                    break;

                case ConsoleKey.NumPad2:
                case ConsoleKey.DownArrow:
                    PlayerMove(0, -1);
                    break;

                case ConsoleKey.NumPad3:
                    PlayerMove(1, -1);
                    break;

                case ConsoleKey.NumPad4:
                case ConsoleKey.LeftArrow:
                    PlayerMove(-1, 0);
                    break;

                case ConsoleKey.NumPad5:
                    Game.WriteMessage("당신은 가만히 있었다.");
                    break;

                case ConsoleKey.NumPad6:
                case ConsoleKey.RightArrow:
                    PlayerMove(1, 0);
                    break;

                case ConsoleKey.NumPad7:
                    PlayerMove(-1, 1);
                    break;

                case ConsoleKey.NumPad8:
                case ConsoleKey.UpArrow:
                    PlayerMove(0, 1);
                    break;

                case ConsoleKey.NumPad9:
                    PlayerMove(1, 1);
                    break;

                case ConsoleKey.C:
                    TryTalk();
                    break;
            }
        }

        public void TryTalk()
        {
            Game.WriteMessage("어느 쪽과 대화할 것인가?");
            ConsoleKeyInfo key = Console.ReadKey(true);

            switch (key.Key)
            {
                case ConsoleKey.NumPad1:
                    TryTalk(-1, -1);
                    break;

                case ConsoleKey.NumPad2:
                case ConsoleKey.DownArrow:
                    TryTalk(0, -1);
                    break;

                case ConsoleKey.NumPad3:
                    TryTalk(1, -1);
                    break;

                case ConsoleKey.NumPad4:
                case ConsoleKey.LeftArrow:
                    TryTalk(-1, 0);
                    break;

                case ConsoleKey.NumPad5:
                    TryTalk(0, 0);
                    break;

                case ConsoleKey.NumPad6:
                case ConsoleKey.RightArrow:
                    TryTalk(1, 0);
                    break;

                case ConsoleKey.NumPad7:
                    TryTalk(-1, 1);
                    break;

                case ConsoleKey.NumPad8:
                case ConsoleKey.UpArrow:
                    TryTalk(0, 1);
                    break;

                case ConsoleKey.NumPad9:
                    TryTalk(1, 1);
                    break;

                default:
                    Game.WriteMessage("잘못된 키 입력");
                    break;
            }
        }
        public void TryTalk(int dx, int dy)
        {
            if (Game.GetByPosition(X + dx, Y + dy) == null)
                Game.WriteMessage("그곳에는 아무도 없다.");
            else if (Game.GetByPosition(X + dx, Y + dy) is Stair)
                Game.WriteMessage("그곳에는 아무도 없다.");
            else
                Game.GetByPosition(X + dx, Y + dy).Chat();
        }

        public void PlayerMove(int dx, int dy)
        {
            bool moved = TryMove(dx, dy);
            if (moved == false)
            {
                GameObject target = Game.GetByPosition(X + dx, Y + dy);

                if (target is Wall)
                    Game.WriteMessage("당신은 벽에 부딪혔다!");
                else if (target is Stair)
                {
                    Game.WriteMessage("당신은 다음 층으로 내려갔다.");


                    Game.MoveToMap(Map.CreateMilkshakeMap());
                }
                else
                {
                    Attack(target);
                }
            }
        }

        public void Equip(EquipmentBase equipment)
        {
            Unequip(equipment.Slot);
            EquippedItems[equipment.Slot] = equipment;
        }

        public void Unequip(EquipmentSlot slot)
        {
            EquippedItems[slot]?.Unequip();
        }
    }
}
