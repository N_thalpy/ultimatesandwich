﻿using System;

namespace UltimateSandwich
{
    public abstract class GameObject
    {
        protected static Random rd;

        public int HP;
        public int MaxHP;

        public int X;
        public int Y;

        public ConsoleColor Foreground
        {
            get;
            protected set;
        }
        public ConsoleColor Background
        {
            get;
            protected set;
        }
        public String DisplayName
        {
            get;
            protected set;
        }
        public String RealName
        {
            get;
            protected set;
        }

        public Dice ToHitDice;
        public Dice DamageDice;
        public Dice DefenseDice;
        public Dice AvoidDice;

        static GameObject()
        {
            rd = new Random();
        }
        public GameObject()
        {
            ToHitDice = new Dice(0, 0, 0);
            DamageDice = new Dice(0, 0, 0);
            DefenseDice = new Dice(0, 0, 0);
            AvoidDice = new Dice(0, 0, 0);
        }

        public abstract void Chat();
        public abstract void Update();

        public void Display()
        {
            Console.ForegroundColor = Foreground;
            Console.BackgroundColor = Background;
            Console.Write(DisplayName);
        }

        protected bool TryMove(int dx, int dy)
        {
            if (Game.GetByPosition(X + dx, Y + dy) == null)
            {
                X += dx;
                Y += dy;

                return true;
            }

            return false;
        }
        protected void Attack(GameObject target)
        {
            int avoid = ToHitDice.Roll() - target.AvoidDice.Roll();
            int dmg = DamageDice.Roll() - target.DefenseDice.Roll();

            Game.WriteMessage("{0}은 {1}을(를) 공격했다.", this.RealName, target.RealName);

            if (avoid < 0)
                Game.WriteMessage("{0}은 {1}의 공격을 회피했다.", target.RealName, this.RealName);
            else
            {
                Game.WriteMessage("{0}은 {1}에게 {2}의 대미지를 주었다.", this.RealName, target.RealName, dmg);

                target.HP -= dmg;
                if (target.HP <= 0)
                    Game.WriteMessage("{0}은 {1}을(를) 파괴했다.", this.RealName, target.RealName);
            }
        }

        public void GotoRandomPosition(Map m)
        {
            int x, y;
            do
            {
                x = rd.Next(0, Game.MapWidth);
                y = rd.Next(0, Game.MapHeight);
            } while (m.GetByPosition(x, y) != null);

            X = x;
            Y = y;
        }
        public void GotoRandomPosition()
        {
            GotoRandomPosition(Game.GetMap());
        }
    }
}
