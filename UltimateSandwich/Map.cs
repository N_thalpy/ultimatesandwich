﻿using System.Collections.Generic;
using UltimateSandwich.Unit;

namespace UltimateSandwich
{
    public sealed class Map
    {
        public List<GameObject> ObjectList;

        private Map()
        {
            ObjectList = new List<GameObject>();

            // 가장자리 부분을 만든다
            GameObject w = null;
            for (int x = 0; x < Game.MapWidth; x++)
            {
                w = new Wall();
                w.X = x;
                w.Y = 0;
                ObjectList.Add(w);

                w = new Wall();
                w.X = x;
                w.Y = Game.MapHeight - 1;
                ObjectList.Add(w);
            }
            for (int y = 0; y < Game.MapHeight; y++)
            {
                w = new Wall();
                w.X = 0;
                w.Y = y;
                ObjectList.Add(w);

                w = new Wall();
                w.X = Game.MapWidth - 1;
                w.Y = y;
                ObjectList.Add(w);
            }

            w = new Stair();
            w.GotoRandomPosition(this);
            ObjectList.Add(w);
        }
        
        public GameObject GetByPosition(int x, int y)
        {
            if (ObjectList.Exists(_ => _.X == x && _.Y == y && _.HP > 0) == false)
                return null;

            return ObjectList.Find(_ => _.X == x && _.Y == y && _.HP > 0);
        }

        // MapFactory 클래스를 만들어서 하는게 정석이겠지만 귀찮으니 그냥 static으로
        public static Map CreateNormalMap()
        {
            return new Map();
        }

        public static Map CreateMilkshakeMap()
        {
            Map m = new Map();

            GameObject obj = new ProphetMilkshake();
            obj.GotoRandomPosition();
            m.ObjectList.Add(obj);


            GameObject amon = new NeutralPatty();
            amon.GotoRandomPosition();
            m.ObjectList.Add(amon);

            GameObject pickle = new SuspiciousPickle();
            pickle.GotoRandomPosition();
            m.ObjectList.Add(pickle);

            GameObject officer = new OfficerCheesestick();
            officer.GotoRandomPosition();
            m.ObjectList.Add(officer);

            return m;
        }
    }
}
