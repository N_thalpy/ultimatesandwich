﻿using System;

namespace UltimateSandwich
{
    public enum EquipmentSlot
    {
        Bread,
        Cabbage,
        Sauce,
        Meat,
    }

    public abstract class EquipmentBase : Item
    {
        public readonly EquipmentSlot Slot;
        public readonly int AGIModifier;
        public readonly int DEFModifier;
        public readonly int ATKModifier;
        Action EquipAction;
        Action UnequipAction;
        public string Name;

        protected EquipmentBase(EquipmentSlot slot, string name, int aGIModifier = 0, int dEFModifier = 0, int aTKModifier = 0,
            Action equipAction = null, Action unequipAction = null)
        {
            Name = name;
            Slot = slot;
            AGIModifier = aGIModifier;
            DEFModifier = dEFModifier;
            ATKModifier = aTKModifier;
            EquipAction = equipAction;
            UnequipAction = unequipAction;
        }

        public void Equip()
        {
            // 핵귀찮
            //Game.GetPlayer().AGI += AGIModifier;
            EquipAction?.Invoke();
        }

        public void Unequip()
        {
            //Game.GetPlayer().AGI -= AGIModifier;
            UnequipAction?.Invoke();
        }
    }
}
