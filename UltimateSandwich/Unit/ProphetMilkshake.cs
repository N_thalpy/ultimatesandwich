﻿using System;
using System.Collections.Generic;

namespace UltimateSandwich.Unit
{
    public sealed class ProphetMilkshake : GameObject
    {
        public sealed class ProphetMilkshakeEncounter : EncounterBase
        {
            public ProphetMilkshakeEncounter() :
                base(
    @"대예언자 밀크셰이크가 당신에게 말을 겁니다.
""자네는 롯데리아의 적이로군... 맞춰 보게. 자네가 여기서 롯데리아 감자튀김 몇 개랑 싸우게 될 것 같나?"""
    , new Dictionary<ConsoleKey, Choice>()
    {
    { ConsoleKey.Q, new Choice("1개", Q) },
    { ConsoleKey.W, new Choice("2개", W) },
    { ConsoleKey.E, new Choice("3개", E) },
    { ConsoleKey.R, new Choice("4개", R) },
    { ConsoleKey.T, new Choice("5개", T) },
    { ConsoleKey.Y, new Choice("6개", Y) },
    { ConsoleKey.U, new Choice("0개", U) },
    })
            {

            }

            static void Q() { Correct(1); }
            static void W() { Correct(2); }
            static void E() { Correct(3); }
            static void R() { Correct(4); }
            static void T() { Correct(5); }
            static void Y() { Correct(6); }

            static void Correct(int number)
            {
                Game.WriteMessage("\"금-빛 정답\"");
                Game.WriteMessage("어디선가 {0}개의 감자 튀김이 나타났다!", number);
                for (int index = 0; index < number; index++)
                {
                    FriedPotato potato = new FriedPotato();
                    potato.GotoRandomPosition();
                    Game.AddUnit(potato);
                }
            }

            static void U()
            {
                Game.WriteMessage("\"그럴 리가 없지!\"");
                Game.WriteMessage("어디선가 {0}개의 감자 튀김이 나타났다!", 8);
                for (int index = 0; index < 8; index++)
                {
                    FriedPotato potato = new FriedPotato();
                    potato.GotoRandomPosition();
                    Game.AddUnit(potato);
                }
            }
        }

        public ProphetMilkshake()
        {
            Foreground = ConsoleColor.DarkGray;
            Background = ConsoleColor.White;

            HP = 20;

            DisplayName = "솈";
            RealName = "대예언자, 밀크셰이크";
            
            AvoidDice = new Dice(5, 10, 0);
        }

        public override void Chat()
        {
            Game.GetPlayer().CurrentEncounter = new ProphetMilkshakeEncounter();
        }
        public override void Update()
        {
        }
    }
}
