﻿using System;
using System.Collections.Generic;

namespace UltimateSandwich.Unit
{
    public class OfficerCheesestick : GameObject
    {
        bool hostile = false;

        public sealed class OfficerCheesestickEncounter : EncounterBase
        {
            public OfficerCheesestickEncounter() :
                base(
    @"롯데리아 만세! 자네는 롯데리아에서 뭐가 가장 맛있다고 생각하나?", new Dictionary<ConsoleKey, Choice>()
    {
    { ConsoleKey.Q, new Choice("\"물론 위대한 AZ버거죠!\"", Q) },
    { ConsoleKey.W, new Choice("\"모짜렐라 인 더 치즈요.\"", W) },
    { ConsoleKey.E, new Choice("\"와퍼\"", ERTY) },
    { ConsoleKey.R, new Choice("\"싸이버거\"", ERTY) },
    { ConsoleKey.T, new Choice("\"1955\"", ERTY) },
    { ConsoleKey.Y, new Choice("\"음... 냅킨?\"", ERTY) },
    })
            {

            }

            static void Q()
            {
                Game.WriteMessage("\"당연하지!\"");
            }

            static void W()
            {
                Game.WriteMessage("\"음.. 그래. 하지만 그 발언은 조심하게. 위대한 AZ버거님만이 유일한 존엄이라네.\"");
            }

            static void ERTY()
            {
                Game.WriteMessage("\"반동이다! 전위대! 전위대!\"");
                for (int index = 0; index < 8; index++)
                {
                    FriedPotato potato = new FriedPotato();
                    potato.GotoRandomPosition();
                    Game.AddUnit(potato);
                }

                ((OfficerCheesestick)Game.GetMap().ObjectList.Find(a => a is OfficerCheesestick)).hostile = true;
            }
        }

        public override void Chat()
        {
            if (hostile) Game.WriteMessage("치즈스틱 경감은 당신을 때리고 있다.");
            else
                Game.GetPlayer().CurrentEncounter = new OfficerCheesestickEncounter();
        }
        public override void Update()
        {
            if (!hostile) return;


            Player pl = Game.GetPlayer();
            int dx = Math.Sign(pl.X - X);
            int dy = Math.Sign(pl.Y - Y);

            bool moved = TryMove(dx, dy);
            if (moved == false)
            {
                if (Game.GetByPosition(X + dx, Y + dy) is Player)
                {
                    GameObject target = Game.GetByPosition(X + dx, Y + dy);
                    Attack(target);
                }
                else
                    TryMove(rd.Next(0, 3) - 1, rd.Next(0, 3) - 1);
            }

        }

        public OfficerCheesestick()
        {
            Foreground = ConsoleColor.Black;
            Background = ConsoleColor.Yellow;

            HP = 180;

            ToHitDice = new Dice(1, 3, 0);
            DamageDice = new Dice(3, 8, 0);

            DisplayName = "치";
            RealName = "치즈스틱 경감";
        }
    }
}
