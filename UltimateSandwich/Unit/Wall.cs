﻿using System;

namespace UltimateSandwich.Unit
{
    public sealed class Wall : GameObject
    {
        public Wall()
        {
            Foreground = ConsoleColor.Black;
            Background = ConsoleColor.Cyan;

            HP = Int32.MaxValue;

            DisplayName = "벽";
            RealName = "벽";
        }

        public override void Chat()
        {
            Game.WriteMessage("당신은 벽과 대화하려 했다.");
        }
        public override void Update()
        {
        }
    }
}
