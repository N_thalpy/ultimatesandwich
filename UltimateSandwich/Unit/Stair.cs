﻿using System;

namespace UltimateSandwich.Unit
{
    public sealed class Stair : GameObject
    {
        public Stair()
        {
            Foreground = ConsoleColor.White;
            Background = ConsoleColor.Black;

            HP = Int32.MaxValue;
            
            DisplayName = "끝";
            RealName = "계단";
        }

        public override void Chat()
        {
            //throw new NotImplementedException();
        }

        public override void Update()
        {
        }
    }
}
