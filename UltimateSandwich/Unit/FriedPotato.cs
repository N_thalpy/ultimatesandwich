﻿using System;

namespace UltimateSandwich.Unit
{
    public sealed class FriedPotato : GameObject
    {
        public FriedPotato()
        {
            Foreground = ConsoleColor.Black;
            Background = ConsoleColor.DarkYellow;

            HP = 40;

            ToHitDice = new Dice(1, 6, 0);
            DamageDice = new Dice(2, 8, 0);

            DisplayName = "감";
            RealName = "감자 튀김";
        }

        public override void Chat()
        {
            Game.WriteMessage("감자 튀김은 울부짖고 있다.");
        }
        public override void Update()
        {
            Player pl = Game.GetPlayer();
            int dx = Math.Sign(pl.X - X);
            int dy = Math.Sign(pl.Y - Y);
            
            bool moved = TryMove(dx, dy);
            if (moved == false)
            {
                if (Game.GetByPosition(X + dx, Y + dy) is Player)
                {
                    GameObject target = Game.GetByPosition(X + dx, Y + dy);
                    Attack(target);
                }
                else
                    TryMove(rd.Next(0, 3) - 1, rd.Next(0, 3) - 1);
            }
        }
    }
}
