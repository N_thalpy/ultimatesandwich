﻿using System;
using System.Collections.Generic;

namespace UltimateSandwich.Unit
{
    public class SuspiciousPickle : GameObject
    {
        public sealed class SuspiciousPickleEncounter : EncounterBase
        {
            public SuspiciousPickleEncounter() :
                base(
    @"수상한 피클이 당신에게 말을 겁니다.
""어이, 샌드위치 형씨, 당신 롯데리아 제품처럼 안 생겼는데, 혹시 반군 동지신가?""
""롯데리아 개※끼 해 봐.""", new Dictionary<ConsoleKey, Choice>()
    {
    { ConsoleKey.Q, new Choice("\"롯데리아!!!!!!!! 개※끼!!!!!!\"", Q) },
    { ConsoleKey.W, new Choice("\"그렇게 절 시험하려 해도 소용없죠, 비밀경찰님. 롯데리아 만세!\"", W) },
    { ConsoleKey.E, new Choice("\"전 비속어를 쓰지 않아요.\"", E) },
    })
            {

            }

            static void Q()
            {
                if (new Dice(1, 3, 0).Roll() == 1)
                {
                    Game.WriteMessage("용기 넘치는 동지로군! 마음에 들어. 이걸 줄 테니 반드시 롯데리아를 무찌르게!");
                }
                else
                {
                    Game.WriteMessage("걸려들었군! 비밀 경찰대! 출동하라!");
                    for (int index = 0; index < 5; index++)
                    {
                        FriedPotato potato = new FriedPotato();
                        potato.GotoRandomPosition();
                        Game.AddUnit(potato);
                    }
                    
                }
                Game.GetMap().ObjectList.RemoveAll(a => a is SuspiciousPickle);
            }

            static void W()
            {
                if (new Dice(1, 3, 0).Roll() == 1)
                {
                    Game.WriteMessage("아닌데? 반군 맞는데? 얘들아, 저놈 잡아라!");
                    for (int index = 0; index < 8; index++)
                    {
                        FriedPotato potato = new FriedPotato();
                        potato.GotoRandomPosition();
                        Game.AddUnit(potato);
                    }
                }
                else
                {
                    Game.WriteMessage("내 정체를 눈치채다니! 보통내기가 아니로군. 여기 롯데리아 특제 소스를 받게!");
                }
                Game.GetMap().ObjectList.RemoveAll(a => a is SuspiciousPickle);
            }

            static void E()
            {
                Game.WriteMessage("흥, 시시한 친구로군.");
            }
        }

        public override void Chat()
        {
            Game.GetPlayer().CurrentEncounter = new SuspiciousPickleEncounter();
        }
        public override void Update()
        {
        }

        public SuspiciousPickle()
        {
            Foreground = ConsoleColor.White;
            Background = ConsoleColor.DarkGreen;

            HP = 180;

            ToHitDice = new Dice(1, 6, 0);
            DamageDice = new Dice(1, 32, 0);

            DisplayName = "피";
            RealName = "수상한 피클";
        }
    }
}
