﻿using System;
using System.Collections.Generic;
using UltimateSandwich.Equipment;

namespace UltimateSandwich.Unit
{
    public class NeutralPatty : GameObject
    {
        public class NeutralPattyEncounter : EncounterBase
        {
            public NeutralPattyEncounter() :
                base(
    @"고기 패티가 당신에게 말을 겁니다.
""이봐요! 얼마 안 있어서 파인애플 피자에 이은 파인애플 버거가 나온대요!""
""어떻게 생각해요?""", new Dictionary<ConsoleKey, Choice>()
    {
    { ConsoleKey.Q, new Choice("\"미친 그게 뭐죠? 극혐\"", QW) },
    { ConsoleKey.W, new Choice("\"와 맛있겠다! 완전 짱인데요?\"", QW) },
    { ConsoleKey.E, new Choice("\"전 밀가루 알레르기 때문에 피자나 버거 맛을 몰라요. 잘 모르겠네요... \"", E) },
    })
            {

            }

            static void QW()
            {
                if (new Dice(1, 2, 0).Roll() == 1)
                {
                    Game.WriteMessage("그렇죠? 역시 제 샌드위치 보는 눈이 맞았네요. 절 데려가 줘요!");
                    var nPatty = Game.GetMap().ObjectList.Find(x => x as NeutralPatty != null);
                    Game.GetMap().ObjectList.Remove(nPatty);
                    Game.GetPlayer().Inventory.Add(new TalkingPatty());
                }
                else
                {
                    Game.WriteMessage("뭐라고? 이 이단자 쓰레기 자식. 내가 죽여 버리겠어.");
                    Fight();
                }
            }

            static void E()
            {
                Game.WriteMessage("밀가루 알레르기? 거짓말하지 마요, 쓰레기 샌드위치. 당신 양쪽의 그 빵은 뭐예요 그럼?" +
                    "\n파인애플 좀 좋아할 수도 있고 싫어할 수도 있죠. 그치만 거짓말은 안 돼요. 전 세상에서 거짓말쟁이가 제일 싫어요.");
                Fight();
            }

            static void Fight()
            {
                var nPatty = Game.GetMap().ObjectList.Find(x => x as NeutralPatty != null);
                
                Game.GetMap().ObjectList.Remove(nPatty);
                PattyOfHatred badPatty = new PattyOfHatred()
                {
                    X = nPatty.X,
                    Y = nPatty.Y
                };
                Game.GetMap().ObjectList.Add(badPatty);
            }
        }


        public NeutralPatty()
        {
            Foreground = ConsoleColor.White;
            Background = ConsoleColor.DarkRed;

            HP = 20;

            DisplayName = "패";
            RealName = "수다쟁이 패티";

            AvoidDice = new Dice(5, 10, 0);
        }

        public override void Chat()
        {
            Game.GetPlayer().CurrentEncounter = new NeutralPattyEncounter();
        }
        public override void Update()
        {
        }
    }
}
