﻿using System;

namespace UltimateSandwich.Unit
{
    public sealed class PattyOfHatred : GameObject
    {
        public PattyOfHatred()
        {
            Foreground = ConsoleColor.White;
            Background = ConsoleColor.Red;

            HP = 180;

            ToHitDice = new Dice(1, 6, 0);
            DamageDice = new Dice(1, 32, 0);

            DisplayName = "패";
            RealName = "증오의 패티";
        }

        public override void Chat()
        {
            Game.WriteMessage("증오의 패티는 파인애플에 대한 자신의 의견을 외쳐대고 있다.");
        }
        public override void Update()
        {
            Player pl = Game.GetPlayer();
            int dx = Math.Sign(pl.X - X);
            int dy = Math.Sign(pl.Y - Y);
            
            bool moved = TryMove(dx, dy);
            if (moved == false)
            {
                if (Game.GetByPosition(X + dx, Y + dy) is Player)
                {
                    GameObject target = Game.GetByPosition(X + dx, Y + dy);
                    Attack(target);
                }
                else
                    TryMove(rd.Next(0, 3) - 1, rd.Next(0, 3) - 1);
            }
        }
    }
}
