﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UltimateSandwich
{
    public abstract class EncounterBase
    {
        protected EncounterBase(string encounterInfo, Dictionary<ConsoleKey, Choice> choices)
        {
            this.encounterInfo = encounterInfo;
            this.choices = choices;
        }

        protected Dictionary<ConsoleKey, Choice> choices;
        protected string encounterInfo;

        public void BeginEncounter()
        {
            Game.WriteMessage(encounterInfo);
        }

        public void DisplayChoices()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var kvp in choices)
                sb.AppendLine(String.Format("{0}: {1}", kvp.Key.ToString(), kvp.Value.Description));

            Game.WriteMessage(sb.ToString());
        }

        public bool GiveChoice(ConsoleKey key)
        {
            bool result = choices.TryGetValue(key, out Choice choice);
            if (result)
                choice.Result();
            else
                // 라이트메시지 말고 다른 방법으로 하게 해야 할 듯
                Game.WriteMessage("그 키는 현재 선택지에서 선택할 수 없는 키입니다.");
            return result;
        }
    }
}
